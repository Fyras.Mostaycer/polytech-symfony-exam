<?php

namespace App\Entity;


use App\Repository\FormationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="decimal")
     */
    private $prix;

    // /**
    //  * @ORM\Column(type="datetime", nullable=true)
    //  */
    // private $begin_at;
    
     /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    // /**
    //  * @ORM\ManyToOne(targetEntity=Participant::class)
    //  * @ORM\JoinColumn(nullable=false)
    //  */
    // private $Participant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function gettitre(): ?string
    {
        return $this->titre;
    }

    public function settitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getprix(): ?string
    {
        return $this->prix;
    }

    public function setprix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    // public function getBegin_at(): ?\DateTimeInterface
    // {
      
    //     return $this->begin_at;
    // }

    // public function setBegin_at(?\DateTimeInterface $begin_at): self
    // {
    //     $this->begin_at = $begin_at;

    //     return $this;
    // }

    public function getduree(): ?float
    {
        return $this->duree;
    }
   

    public function setduree(?float $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getParticipant(): ?Participant
    {
        return $this->Participant;
    }

    public function setParticipant(?Participant $Participant): self
    {
        $this->Participant = $Participant;

        return $this;
    }
}
