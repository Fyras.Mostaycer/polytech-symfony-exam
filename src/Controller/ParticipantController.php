<?php

// src/Controller/FormationController.php
namespace App\Controller;

use App\Form\ParticipantType;
use App\Repository\ParticipantRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ParticipantController extends AbstractController
{
    // #[Route('/Participant')]
    // public function ajouter(): Response
    // {

       
    // }
    // #[Route('/Participant')]
    // public function show(): Response
    // {

       
    // }
    #[Route('/participant')]
    public function index(ParticipantRepository $participantRepository): Response
    {
        $participants = $participantRepository->findAll();

        return $this->render('participant/index.html.twig', [
            'participants' => $participants,
        ]);
    }



#[Route('/participant', name: 'Ajouter')]
public function ajouter(Request $request): Response
{
    // Create a new instance of the Participant entity
    $participant = new Participant();

    // Create a new instance of the ParticipantType form
    $form = $this->createForm(ParticipantType::class, $participant);

    // Handle the form submission
    $form->handleRequest($request);

    // If the form was submitted and is valid...
    if ($form->isSubmitted() && $form->isValid()) {

        // Save the Participant entity to the database
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($participant);
        $entityManager->flush();

        // Redirect to a success page
        return $this->redirectToRoute('participant');
    }

    // Render the form view
    return $this->render('participant/ajouter.html.twig', [
        'participantForm' => $form->createView(),
    ]);
}

}