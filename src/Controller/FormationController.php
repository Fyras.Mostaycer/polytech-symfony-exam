<?php

// src/Controller/FormationController.php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\FormationType;
use App\Repository\FormationRepository;
use App\Entity\Formation;

class FormationController extends AbstractController
{
    /**
 * @var EntityManagerInterface
 */
private $entityManager;
public function __construct(EntityManagerInterface  $entityManager)
{
    $this->entityManager = $entityManager;

}
    // #[Route('/formation/{id}')]
    // public function edit(): Response
    // {
    // }
    // #[Route('/formation')]
    // public function addM(): Response
    // {
    // }
    // #[Route('/formation/{id}')]
    // public function delete(): Response
    // {
    // }
    #[Route('/formation')]
     
       public function index(FormationRepository $formationRepository): Response
       {
           $formations = $formationRepository->findAll();
   
           return $this->render('formation/index.html.twig', [
               'formations' => $formations,
           ]);
       }

    

       #[Route('/formation/ajouter', name: 'add_formation')]
       public function addM(Request $request): Response
       {
           // Create a new instance of the Formation entity
           $formation = new Formation();
   
           // Create a new instance of the FormationType form
           $form = $this->createForm(FormationType::class, $formation);
   
           // Handle the form submission
           $form->handleRequest($request);
   
           // If the form was submitted and is valid...
           if ($form->isSubmitted() && $form->isValid()) {
   
               // Save the Formation entity to the database
               $this->entityManager->persist($formation);
               $this->entityManager->flush();
   
               // Redirect to a success page
               return $this->redirectToRoute('app_formation_index');
           }
   
           // Render the form
           return $this->render('formation/addM.html.twig', [
               'form' => $form->createView(),
           ]);
       }
       
    #[Route('/formation/{id}', name: 'edit_formation')]
    public function editM(Request $request, Formation $formation): Response
    {
        // Create a new instance of the FormationType form
        $form = $this->createForm(FormationType::class, $formation);

        // Handle the form submission
        $form->handleRequest($request);

        // If the form was submitted and is valid...
        if ($form->isSubmitted() && $form->isValid()) {

            // Save the Formation entity to the database
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formation);
            $entityManager->flush();

            // Redirect to a success page
            return $this->redirectToRoute('formation');
        }

        // Render the form
        return $this->render('formation/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    
   
}