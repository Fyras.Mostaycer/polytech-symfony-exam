<?php

namespace Container4LJLTjC;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getPieceTypeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Form\PieceType' shared autowired service.
     *
     * @return \App\Form\PieceType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/PieceType.php';

        return $container->privates['App\\Form\\PieceType'] = new \App\Form\PieceType();
    }
}
