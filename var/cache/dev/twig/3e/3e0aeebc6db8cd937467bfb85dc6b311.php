<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* montage/index.html.twig */
class __TwigTemplate_4ad62d894f0790c513581ff5327b931b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "montage/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "montage/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "montage/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"table-responsive\">
  <table class=\"table\">
  <h1 class='text-center'>Liste de Montages</h1>
  <thead>
    <tr>
        <th>id</th>
        <th>nomIMontage</th>
        <th>client</th>
        ";
        // line 13
        echo "        <th>cout</th>
        <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    
    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["montages"]) || array_key_exists("montages", $context) ? $context["montages"] : (function () { throw new RuntimeError('Variable "montages" does not exist.', 19, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["montage"]) {
            // line 20
            echo "                <tr>
                    <th scope=\"row\">";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["montage"], "id", [], "any", false, false, false, 21), "html", null, true);
            echo "</th>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["montage"], "nomlMontage", [], "any", false, false, false, 22), "html", null, true);
            echo "</td>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["montage"], "client", [], "any", false, false, false, 23), "html", null, true);
            echo "</td>
                    ";
            // line 25
            echo "                    <td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["montage"], "cout", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
                   <td>
                        ";
            // line 28
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_montage", ["id" => twig_get_attribute($this->env, $this->source, $context["montage"], "id", [], "any", false, false, false, 28)]), "html", null, true);
            echo "\" class='btn btn-sm btn-primary ml-2' >edit</a>
                    </td>

                </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['montage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("add_montage");
        echo "\" class='btn btn-sm btn-primary  d-flex justify-content-center align-items-center'>ajouter un montage</a>
    
</table>
</div>
    
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "montage/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 33,  111 => 28,  105 => 25,  101 => 23,  97 => 22,  93 => 21,  90 => 20,  86 => 19,  78 => 13,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"table-responsive\">
  <table class=\"table\">
  <h1 class='text-center'>Liste de Montages</h1>
  <thead>
    <tr>
        <th>id</th>
        <th>nomIMontage</th>
        <th>client</th>
        {# <th>created_at</th> #}
        <th>cout</th>
        <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    
    {% for montage in montages %}
                <tr>
                    <th scope=\"row\">{{ montage.id }}</th>
                    <td>{{ montage.nomlMontage }}</td>
                    <td>{{ montage.client }}</td>
                    {# <td>{{ montage.created_at }}</td> #}
                    <td>{{ montage.cout }}</td>
                   <td>
                        {# <a href=\"{{ path('montage_show', {'id': montage.id}) }}\">show</a> #}
                        <a href=\"{{ path('edit_montage', {'id': montage.id}) }}\" class='btn btn-sm btn-primary ml-2' >edit</a>
                    </td>

                </tr>
    {% endfor %}
    <a href=\"{{ path('add_montage' ) }}\" class='btn btn-sm btn-primary  d-flex justify-content-center align-items-center'>ajouter un montage</a>
    
</table>
</div>
    
{% endblock %}", "montage/index.html.twig", "/Users/takiacademy/Desktop/Sym/templates/montage/index.html.twig");
    }
}
